const { expect } = require('@jest/globals');
const sumar = require('./sumar');

test('sum ok',() => {
  expect(sumar(1,2)).toBe(3)
  expect(sumar(2,3)).toBe(4)
})